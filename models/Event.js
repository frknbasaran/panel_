var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

module.exports = new Schema({
    name: {
        type: String
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'EventType'
    },
    sponsor: {
        type: Schema.Types.ObjectId,
        ref: 'Brand'
    },
    img: {
        type: String
    },
    place: {
        type: String
    },
    description: {
        type: String
    },
    nowBuy: {
      type: Number
    },
    reservation_distance: {
        type:Number
    },
    event_date: {
        type: Date
    },
    reservation_date: {
        type: Date
    },
    ticket_sell_start_date: {
        type: Date
    },
    ticket_count: {
        type: Number
    },
    reservation_count: {
        type: Number
    },
    featured: {
        type: Boolean,
        default: false
    }
});





