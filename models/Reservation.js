var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

module.exports = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    event: {
        type: Schema.Types.ObjectId,
        ref: 'Event'
    },
    end_date: {
        type: Date
    }
});
