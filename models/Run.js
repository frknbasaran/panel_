var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

module.exports = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    start_time: {
        type: Date
    },
    end_time: {
        type: Date
    },
    distance: {
        type: Number
    }
});
