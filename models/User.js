var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    md5 = require('md5'),
    User;

module.exports = new Schema({
    name: {
        type: String
    },
    username: {
        type: String
    },
    email: {
        type: String
    },
    auth_level: {
        type: Number,
        default: 1
    },
    password: {
        type: String,
        required: true
    },
    registered_at: {
        type: Date,
        default: Date.now
    },
    gender: {
        type: String
    },
    twitter: {
        type: String
    },
    facebook: {
        type: String
    },
    balance: {
        type: Number,
        default: 0
    },
    prphoto: {
        type: String,
        required: false
    }
});