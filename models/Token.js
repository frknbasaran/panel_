var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    Token;

module.exports = new Schema({
    token: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    ip: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});
