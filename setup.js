/*
 * @author: Furkan Başaran <frknbasaran@gmail.com>
 * @date: 29.10.2015
 *
 * Application basic dependencies initalize script
 * */

var sys = require('sys')
var exec = require('child_process').exec;

var mode = process.argv[2];

if (mode == "full") {
    console.log("Newspaper IO - @FurkanBaşaran");
    console.log("Kurulum başlıyor...");
    console.log("Uygulama Back-End gereklilikleri yükleniyor..");
    console.log("Bu aşama SUDO yetkisi gerektirdiğinden şifre girmeniz gerekebilir")
    exec("sudo npm install", function (error, stdout, stderr) {

        console.log(stdout);

        var mongoose = require('mongoose');
        var config = require('./config');
        var md5 = require('md5');

        mongoose.connect(config.database.connectionString);

        var UserSchema = require('./models/User');
        var CategorySchema = require('./models/Brand');
        var RoleSchema = require('./models/Reservation');


        /*
         * Create standart user roles
         * */
        console.log("Adım 1: Roller Oluşturuluyor..");

        /*
         * Create Mongoose Model from Schema
         * */
        var RoleModel = mongoose.model('Role', RoleSchema);

        /*
         * Create Role Instances
         * */
        var Removed = new RoleModel({"name": "Uçurulmuş"});
        var Author = new RoleModel({"name": "Yazar"});
        var Moderator = new RoleModel({"name": "Moderatör"});
        var Admin = new RoleModel({"name": "Admin"});

        /*
         * Define an array object includes roles
         * */
        var Roles = [Author, Moderator, Admin, Removed];

        /*
         * Save standard roles to database
         * */
        Roles.forEach(function (Role) {
            Role.save(function (err, role) {
                if (!err) console.log("Rol oluşturuldu: " + role.name);
                else console.log("Rol oluşturulamadı, hata: " + err)
            })
        })

        /*
         * Create Root Category Object
         * */
        console.log("Adım 2: Root Kategori Oluşturuluyor..")

        /*
         * Create Mongoose Model from Schema
         * */
        var CategoryModel = mongoose.model('Category', CategorySchema);

        /*
         * Create Root Category Object
         * */
        var Root = new CategoryModel({"name": "Kök Kategori"});

        /*
         * Save category object into database
         * */
        Root.save(function (err, root) {
            if (!err) console.log("Kategori oluşturuldu:" + root.name)
            else console.log("Kategori oluşturulamadı, hata:" + err);
        })

        /*
         * Create a admin user for first logIn
         * */
        console.log("Adım 3: Sistem Yöneticisi hesabı oluşturuluyor..")

        /*
         * Create Mongoose Model from Schema
         * */
        var UserModel = mongoose.model('User', UserSchema);

        /*
         * Create User with admin privileges
         * */
        var SystemAdmin = new UserModel({
            "name": "Sistem Yöneticisi",
            "username": "sysAdmin",
            "email": "root@nedzia.com",
            "password": md5('mzooryhcfngw'),
            'role': Admin
        });

        /*
         * Save System Admin user into database
         * */
        SystemAdmin.save(function (err, sysadmin) {
            if (!err) console.log("Sistem Yöneticisi oluşturuldu, \nEmail: root@nedzia.com\nUsername: sysAdmin");
        })

        console.log("Kurulum tamamlandı.");
        console.log("Uygulama Front-End gereklilikleri yükleniyor..");
        exec("cd public && sudo sh ./setup.sh", function (error, stdout, stderr) {
            exec("cd .. && sudo mkdir database", function (error, stdout, stderr) {
                exec("mongod --dbpath database", function (error, stdout, stderr) {
                    console.log("Uygulama ayağa kaldırılıyor")
                    exec("sudo nodemon server")
                })
            });
        });
    });
} else if (mode == "db") {
    var mongoose = require('mongoose');
    var config = require('./config');
    var md5 = require('md5');

    mongoose.connect(config.database.connectionString);

    var UserSchema = require('./models/User');
    var CategorySchema = require('./models/Brand');
    var RoleSchema = require('./models/Reservation');


    /*
     * Create standart user roles
     * */
    console.log("Adım 1: Roller Oluşturuluyor..");

    /*
     * Create Mongoose Model from Schema
     * */
    var RoleModel = mongoose.model('Role', RoleSchema);

    /*
     * Create Role Instances
     * */
    var Author = new RoleModel({"name": "Yazar"});
    var Moderator = new RoleModel({"name": "Moderatör"});
    var Admin = new RoleModel({"name": "Admin"});

    /*
     * Define an array object includes roles
     * */
    var Roles = [Author, Moderator, Admin];

    /*
     * Save standard roles to database
     * */
    Roles.forEach(function (Role) {
        Role.save(function (err, role) {
            if (!err) console.log("Rol oluşturuldu: " + role.name);
            else console.log("Rol oluşturulamadı, hata: " + err)
        })
    })

    /*
     * Create Root Category Object
     * */
    console.log("Adım 2: Root Kategori Oluşturuluyor..")

    /*
     * Create Mongoose Model from Schema
     * */
    var CategoryModel = mongoose.model('Category', CategorySchema);

    /*
     * Create Root Category Object
     * */
    var Root = new CategoryModel({"name": "Kök Kategori"});

    /*
     * Save category object into database
     * */
    Root.save(function (err, root) {
        if (!err) console.log("Kategori oluşturuldu:" + root.name)
        else console.log("Kategori oluşturulamadı, hata:" + err);
    })

    /*
     * Create a admin user for first logIn
     * */
    console.log("Adım 3: Sistem Yöneticisi hesabı oluşturuluyor..")

    /*
     * Create Mongoose Model from Schema
     * */
    var UserModel = mongoose.model('User', UserSchema);

    /*
     * Create User with admin privileges
     * */
    var SystemAdmin = new UserModel({
        "name": "Sistem Yöneticisi",
        "username": "sysAdmin",
        "email": "root@nedzia.com",
        "password": md5('mzooryhcfngw'),
        'role': Admin
    });

    /*
     * Save System Admin user into database
     * */
    SystemAdmin.save(function (err, sysadmin) {
        if (!err) console.log("Sistem Yöneticisi oluşturuldu, \nEmail: root@nedzia.com\nUsername: sysAdmin");
        exec("exit");
    })
}

