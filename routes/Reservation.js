var ReservationSchema = require('../models/Reservation');

module.exports = function(restful, app) {

  var ReservationResource = restful.model('Reservation', ReservationSchema)
      .methods(["get", "post", "put", "delete"])


  ReservationResource
      .register(app, '/api/reservations');

}
