var RunSchema = require('../models/Run');

module.exports = function (restful, app) {

    var RunResource = restful.model('Run', RunSchema)
        .methods(["get", "post", "put", "delete"]);

    RunResource
        .register(app, '/api/runs');

}

