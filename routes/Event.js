var EventSchema = require('../models/Event');


module.exports = function (restful, app) {

    var EventResource = restful.model('Event', EventSchema)
        .methods(["get", "post", "put", "delete"])

    EventResource
        .route('featured.get', function (req, res, next) {
            EventResource
                .count({"featured":true})
                .exec(function(err, count) {
                    if (!err && count > 2) {
                        EventResource
                            .find({"featured":true})
                            .exec(function(err, events) {
                                if (err) res.json({"error":true, "message":"somethings went wrong"});
                                else res.json({"error":false, "data":events});
                            })
                    }
                })
        })

    EventResource
        .register(app, '/api/events');

}
