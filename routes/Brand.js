var BrandSchema = require('../models/Brand');

module.exports = function (restful, app) {

    var BrandResource = restful.model('Brand', BrandSchema)
        .methods(["get", "post", "put", "delete"]);

    BrandResource
        .before('put', function(req, res, next) {
            console.log(req.body);
            next();
        })

    BrandResource
        .register(app, '/api/brands');

}
