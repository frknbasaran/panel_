var TicketSchema = require('../models/Ticket');
var moment = require('moment');

module.exports = function (restful, app) {

    var TicketResource = restful.model('Ticket', TicketSchema)
        .methods(["get", "post", "put", "delete"]);

    TicketResource
        .after('get', function(req, res, next) {
            res.locals.bundle.forEach(function(ticket, idx, tickets) {
                tickets[idx] = ticket.toObject();
                tickets[idx].event_date = moment(ticket.event_date).fromNow()
            })
            next();
        })


    TicketResource
        .register(app, '/api/tickets');

}
