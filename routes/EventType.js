var EventTypeSchema = require('../models/EventType');

module.exports = function (restful, app) {

    var EventTypeResource = restful.model('EventType', EventTypeSchema)
        .methods(["get", "post", "put", "delete"])

    EventTypeResource
        .register(app, '/api/event-types');

}
