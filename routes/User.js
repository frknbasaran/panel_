var UserSchema = require('../models/User');
var TicketSchema = require('../models/Ticket');
var md5 = require('md5');
var Constants = require('../Constants');
var moment = require('moment');

moment.locale('tr');

module.exports = function (restful, app, mongoose) {

    var UserResource = restful.model('User', UserSchema)
        .methods(["get", "post", "put", "delete"])

    var UserHelper = require('../core/User')(UserResource, TicketSchema, mongoose);

    /*
     * Name: Register Service
     * Path: /users
     * Required Params:
     *   String nameSurname,
     *   String email,
     *   String password,
     * */
    UserResource
        .before('post', function (req, res, next) {
            UserHelper
                .emailCheck(req.body.email, req.body.name)
                .then(function (count) {
                    req.body.password = md5(req.body.password);
                    req.body.prphoto = "http://gravatar.com/avatar/" + md5(req.body.email);

                    count > 0 ? res.json({"error": true, "message": Constants.register.email_already_using}) : next();
                })
                .catch(function (err) {
                    res.json({"error": true, "message": err.message})
                })

        });

    UserResource
        .route('delete.post', function (req, res, next) {
            UserHelper
                .deleteUserAndUpdateRole(req.body.id)
                .then(function (result) {
                    if (!result.error) res.json({"error": false, "data": "user deleted successfuly"});
                    else res.json({"error": true, "message": "somethings went wrong"});
                })
                .catch(function (err) {
                    res.json({"error": true, "message": err.message})
                })
        })

    /*
     * Name: Login Service
     * Path: /users/login
     * Required Params:
     *   String email,
     *   String password,
     * */
    UserResource
        .route('login.post', function (req, res) {

            UserHelper
                .logIn(req.body.email, req.body.password)
                .then(function (result) {
                    if (!result.error) res.json({"error": false, "data": result.data});
                    else res.json({"error": true, "message": "Invalid credentials"});
                })
                .catch(function (err) {
                    res.json({"error": true, "message": err.message})
                })
        })


    /*
     * Name: Login Service
     * Path: /users/signin
     * Required Params:
     *   String email,
     *   String password,
     * */
    UserResource
        .route('signin.post', function (req, res) {
            UserHelper
                .signIn(req.body.email, req.body.password)
                .then(function (result) {
                    if (!result.error) res.json({"error": false, "data": result.data});
                    else res.json({"error": true, "message": "Invalid credentials"});
                })
                .catch(function (err) {
                    res.json({"error": true, "message": err.message})
                })
        })


    var encrypt_password = function (req, res, next) {
        if (req.body.password) req.body.password = md5(req.body.password);
    }

    UserResource
        .register(app, '/api/users');

}

