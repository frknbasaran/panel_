var config = require('../config'),
    knex = require('knex')(config.database),
    Bookshelf = require('bookshelf')(knex),

    Token = require('../models/Token'),
    User = require('../models/User');


var Tokens = Bookshelf.Collection.extend({
    model: Token
});

var Users = Bookshelf.Collection.extend({
    model: User
});

module.exports = function (req, res, next) {

    /*

    var tokenKey = req.headers.ptoken;
    var currentUser = req.headers.user;
    var remoteAddr = req.ip;

    Token
        .forge({token: tokenKey})
        .fetch()
        .then(function (token) {
            if (token && token.attributes.user == currentUser && token.attributes.ip == remoteAddr) {
                next();
            } else res.sendStatus(403);
        })
        .otherwise(function (err) {
            res.status(500).json({error: true, data: {message: err.message}});
        })
     * */

    console.log("İstek geldi araya girdim");

    if (req.headers.onur != undefined) next();
    else res.sendStatus(403);

}
