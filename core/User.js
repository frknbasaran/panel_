var promise = require('bluebird');
var md5 = require('md5');
var TokenSchema = require('../models/Token');
var ObjectID = require('mongodb').ObjectID;

module.exports = function (UserModel, TicketModel, mongoose) {
    return {
        /*
        * for user login
        * */
        "signIn": function (email, password, ipAddr) {

            var deferred = promise.pending();

            UserModel
                .find()
                .where('email').equals(email)
                .where('password').equals(md5(password))
                .exec(function (err, user) {

                    if (!err && user.length > 0) {

                        var Token = mongoose.model('Token', TokenSchema);

                        Token
                            .find()
                            // usera ait token varmı
                            .where('user').equals(user[0]._id)
                            .exec(function (err, token) {

                                if (!err && token.length > 0) {

                                    deferred.fulfill({
                                        "error": false,
                                        "data": {"user": user[0], "token": token[0], "ip": token[0].ip}
                                    })
                                } else {
                                    // yoksa  yeni token oluştur
                                    var generatedToken = new Token();
                                    //random token id oluştur
                                    var uid = require('rand-token').uid;

                                    var tokenIp = ipAddr;
                                    var tokenString = uid(32);

                                    // oluşturulan token nesnesinin useri  ilk  sorgudan gelen  userid si yap
                                    var userId = new ObjectID(user[0]._id);

                                    generatedToken.user = userId;
                                    generatedToken.token = tokenString;// token stringi ekle
                                    generatedToken.ip = tokenIp;

                                    TicketModel
                                        .find({"user":generatedToken.userId})
                                        .then(function(err, ticket) {
                                            
                                        })




                                    generatedToken.save(function (err, token) {
                                        if (!err) deferred.fulfill({
                                            "error": false,
                                            "data": {"user": user[0], "token": token, "ip": token.ip}
                                        })
                                    })
                                }
                            })

                    }
                    else deferred.reject({"error": "true", "message": "Kullanıcı bilgileri hatalı"});
                })

            return deferred.promise;

        },

        "logIn": function (email, password, ipAddr) {

            var deferred = promise.pending();

            UserModel
                .find()
                .where('email').equals(email)
                .where('password').equals(md5(password))
                .exec(function (err, user) {

                    if ((!err && user.length > 0) && user[0].auth_level == 2) {

                        var Token = mongoose.model('Token', TokenSchema);

                        Token
                            .find()
                            // usera ait token varmı
                            .where('user').equals(user[0]._id)
                            .exec(function (err, token) {

                                if (!err && token.length > 0) {

                                    deferred.fulfill({
                                        "error": false,
                                        "data": {"user": user[0], "token": token[0], "ip": token[0].ip}
                                    })
                                } else {
                                    // yoksa  yeni token oluştur
                                    var generatedToken = new Token();
                                    //random token id oluştur
                                    var uid = require('rand-token').uid;

                                    var tokenIp = ipAddr;
                                    var tokenString = uid(32);

                                    // oluşturulan token nesnesinin useri  ilk  sorgudan gelen  userid si yap
                                    var userId = new ObjectID(user[0]._id);

                                    generatedToken.user = userId;
                                    generatedToken.token = tokenString;// token stringiniekle
                                    generatedToken.ip = tokenIp;


                                    generatedToken.save(function (err, token) {
                                        if (!err) deferred.fulfill({
                                            "error": false,
                                            "data": {"user": user[0], "token": token, "ip": token.ip}
                                        })
                                    })
                                }
                            })

                    }
                    else deferred.reject({"error": "true", "message": "Kullanıcı bilgileri hatalı"});
                })

            return deferred.promise;

        },
        'emailCheck': function (email, name) {

            var deferred = promise.pending();

            Model
                .count({"email": email})
                .exec(function (err, count) {
                    deferred.fulfill(count);
                })

            return deferred.promise;
        },

        'deleteUserAndUpdateRole': function (userId) {

            var deferred = promise.pending();

            Model
                .findById(userId)
                .exec(function (err, user) {
                    if (!err) {
                        user.auth = 0;
                        user.save(function (err, savedUser) {
                            deferred.fulfill(savedUser);
                        });
                    } else deferred.reject({"error": true})

                })

            return deferred.promise;


        }


    }
}