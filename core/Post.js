var promise = require('bluebird');
var md5 = require('md5');
var ObjectID = require('mongodb').ObjectID;
module.exports = function (Model, mongoose) {

    return {
        'viewCountAdd': function (postId) {

            var deferred = promise.pending();

            Model
                .findOne()
                .where('_id').equals(postId)
                .exec(function (err, data) {

                    if (!err) {
                        if(data) {
                            data.view_count = parseInt(data.view_count) + 1;
                            data.save(function (err, savedData) {
                                if (!err) deferred.fulfill({"error": false, "data": savedData});
                            })
                        } else deferred.reject({"error": true, "message": "Eşleşen data yok."})

                    } else deferred.reject({"error": true, "message": err.message})

                })

            return deferred.promise;
        }
    }
}