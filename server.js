/*
 @author: Furkan BAŞARAN
 @date: 10.08.2015
 */
var express = require('express'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    restful = require('node-restful'),
    config = require('./config'),
    app = express();

mongoose.connect(config.database.connectionString);

app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(methodOverride());


app.use('/', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", ["GET", "POST", "DELETE", "PUT", "OPTIONS"]);
    next();
});

require('./routes/User')(restful, app, mongoose);
require('./routes/Event')(restful, app);
require('./routes/EventType')(restful, app);
require('./routes/Brand')(restful, app);
require('./routes/Reservation')(restful, app);
require('./routes/Run')(restful, app);
require('./routes/Ticket')(restful, app);


app.set('port', config.port);
app.use(express.static('public'));

app.listen(app.get('port'), function () {
    console.log("✔ Application alive on %d in %s mode", app.get('port'), app.get('env'));
});
