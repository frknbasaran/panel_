app.controller('UserController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("User Controller worked.")
    
    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $scope.newUser = {
        "name": "",
        "username": "",
        "email": "",
        "password": "",
        "auth_level": 0
    }

    $scope.isLoading = true;
    $http.get('/api/users?sort=registered_at')
        .then(function (response) {
            $scope.isLoading = false;
            $scope.users = response.data;
        })

    $scope.addNewUser = function () {

        $http({
            url: "/api/users",
            method: "POST",
            data: JSON.stringify($scope.newUser),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.users.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Bu e-posta ya da isimde kullanıcı mevcut.";
                }
            })
    }

    $scope.updateUser = function (user) {

        var userWillBeUpdated = {
            name: user.name,
            username: user.username,
            auth_level: user.auth_level,
            email: user.email,
            twitter: user.twitter,
            facebook: user.facebook,
            balance: user.balance
        }

        $http({
            url: "/api/users/" + user._id,
            method: "PUT",
            data: JSON.stringify(userWillBeUpdated),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.successMessage = "Başarıyla Güncellendi";
                    $scope.errorMessage = undefined;
                }
            })
    }

    $scope.remove = function (user) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/users/" + user._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.users.splice($scope.users.indexOf(user), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);