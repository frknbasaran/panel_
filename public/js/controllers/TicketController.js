app.controller('TicketController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("Ticket Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $scope.newTicket = {
        "event": "",
        "user": ""
    }

    $http.get('/api/users')
        .then(function (response) {
            $scope.users = response.data;
        })

    $http.get('/api/events')
        .then(function (response) {
            $scope.events = response.data;
        })

    $http.get('/api/tickets?populate=event&populate=user')
        .then(function (response) {
            $scope.tickets = response.data;
        })

    $scope.addNewTicket = function () {

        $http({
            url: "/api/tickets",
            method: "POST",
            data: JSON.stringify($scope.newTicket),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.tickets.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Bir şeyler ters gitti.";
                }
            })
    }

    $scope.updateTicket = function (ticket) {

        $http({
            url: "/api/tickets/" + ticket._id,
            method: "PUT",
            data: JSON.stringify(reservation),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.successMessage = "Başarıyla Güncellendi";
                    $scope.errorMessage = undefined;
                }
            })
    }

    $scope.remove = function (ticket) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/tickets/" + ticket._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.tickets.splice($scope.tickets.indexOf(ticket), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);