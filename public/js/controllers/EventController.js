app.controller('EventController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("Event Controller worked.")


    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $http({
        method: "GET",
        url: '/api/brands'
    }).then(function (response) {
        $scope.brands = response.data;
    })

    $http({
        method: "GET",
        url: '/api/event-types'
    }).then(function (response) {
        $scope.eventTypes = response.data;
    })

    $scope.newEvent = {
        "name": "",
        "type": "",
        "sponsor": "",
        "place": "",
        "description": "",
        "event_date": "",
        "reservation_date": "",
        "sell_start_date": "",
        "nowBuy": 0,
        "reservation_distance": 0,
        "ticket_count": 0,
        "reservation_count": 0
    }

    $http.get('/api/events?populate=type&populate=sponsor&sort=-event_date')
        .then(function (response) {
            $scope.events = response.data;
        })

    $scope.addNewEvent = function () {
        if (!$scope.validation($scope.newEvent)) return;
        $http({
            url: "/api/events",
            method: "POST",
            data: JSON.stringify($scope.newEvent),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.events.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Birşeyler ters gitti.";
                }
            })
    }

    $scope.validation = function (data) {
        if (data.reservation_date == null || data.event_date == null || data.ticket_sell_start_date == null) {
            $scope.successMessage = undefined;
            $scope.errorMessage = "Lütfen gerekli zaman aralıklarını doldurun";
            return false;
        }

        if (data.event_date < data.ticket_sell_start_date) {
            $scope.successMessage = undefined;
            $scope.errorMessage = "Etkinlik tarihi, Bilet satış başlangıç tarihinden sonra olmalıdır";
            return false;
        }

        if (data.event_date < data.reservation_date) {
            $scope.successMessage = undefined;
            $scope.errorMessage = "Etkinlik tarihi, Rezervasyon bitiş tarihinden sonra olmalıdır";
            return false;
        }

        if (data.ticket_sell_start_date < data.reservation_date) {
            $scope.successMessage = undefined;
            $scope.errorMessage = "Bilet satış başlangıç tarihi, Rezervasyon bitiş tarihinden sonra olmalıdır";
            return false;
        }
        return true;
    }


    $scope.updateEvent = function (event) {
        if (event.event_date > event.ticket_date && event.ticket_date > event.reservation_date) {
            var eventWillBeUpdated = {
                name: event.name,
                type: event.type,
                place: event.place,
                sponsor: event.sponsor,
                description: event.description
            }

            $http({
                url: "/api/events/" + event._id,
                method: "PUT",
                data: JSON.stringify(eventWillBeUpdated),
                headers: {"content-type": "application/json"}
            })
                .then(function (response) {
                    if (response.status == 200) {
                        $scope.successMessage = "Başarıyla Güncellendi";
                        $scope.errorMessage = undefined;
                    }
                })
            event.editing = false;
        } else {
            $scope.errorMessage = "Bilet satış başlangıç tarihi, Rezervasyon bitiş tarihinden sonra olmalıdır";
            return false;
            event.editing = true;
        }

    }

    $scope.remove = function (event) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/events/" + event._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.users.splice($scope.events.indexOf(event), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);