app.controller('EventTypeController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("EventType Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $scope.newEventType = {
        "name": ""
    }

    $http.get('/api/event-types')
        .then(function (response) {
            $scope.eventTypes = response.data;
        })

    $scope.addNewEventType = function () {

        $http({
            url: "/api/event-types",
            method: "POST",
            data: JSON.stringify($scope.newEventType),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.eventTypes.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Bir şeyler ters gitti.";
                }
            })
    }

    $scope.updateEventType = function (type) {

        $http({
            url: "/api/event-types/" + type._id,
            method: "PUT",
            data: JSON.stringify(type),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.successMessage = "Başarıyla Güncellendi";
                    $scope.errorMessage = undefined;
                }
            })
    }

    $scope.remove = function (type) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/event-types/" + type._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.eventTypes.splice($scope.eventTypes.indexOf(type), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);