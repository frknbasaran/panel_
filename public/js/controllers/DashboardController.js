app.controller('DashboardController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("Dashboard Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $http.get('/api/users?sort=-registered_at')
        .then(function (response) {
            $scope.usersCount = response.data.length;

            $scope.admins = [];
            response.data.forEach(function (user) {
                if (user.auth_level == 2) $scope.admins.push(user);
            })

            $scope.last3Users = [];
            $scope.counter = 0;
            response.data.forEach(function (user) {

                if ($scope.counter < 3) {
                    $scope.last3Users.push(user);
                    $scope.counter++;
                }
            })


        })

    $http.get('/api/tickets?populate=user&populate=event')
        .then(function (response) {
            $scope.ticketsCount = response.data.length;

            $scope.last3Tickets = [];
            $scope.counter = 0;
            response.data.forEach(function (ticket) {

                if ($scope.counter < 3) {
                    $scope.last3Tickets.push(ticket);
                    $scope.counter++;
                }
            })
        })


    $http.get('/api/brands')
        .then(function (response) {
            $scope.brandsCount = response.data.length;
        })

    $http.get('/api/events?sort=-event_date')
        .then(function (response) {
            $scope.eventsCount = response.data.length;

            $scope.last3Events = [];
            $scope.counter = 0;
            response.data.forEach(function (event) {

                if ($scope.counter < 3) {
                    $scope.last3Events.push(event);
                    $scope.counter++;
                }
            })
        })

    $http.get('/api/runs')
        .then(function (response) {
            $scope.runsCount = response.data.length;
            $scope.totalKM = 0;
            response.data.forEach(function (run) {
                $scope.totalKM += run.distance;
            })
        })


}]);