app.controller('HeaderController', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {

    console.log("Header Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = JSON.parse(localStorage.getItem("user"));
    }
}]);