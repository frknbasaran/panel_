app.controller('BrandController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("Brand Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $scope.newBrand = {
        "name": ""
    }

    $http.get('/api/brands')
        .then(function (response) {
            $scope.brands = response.data;
        })

    $scope.addNewBrand = function () {

        $http({
            url: "/api/brands",
            method: "POST",
            data: JSON.stringify($scope.newBrand),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.brands.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Bir şeyler ters gitti.";
                }
            })
    }

    $scope.updateBrand = function (brand) {

        $http({
            url: "/api/brands/" + brand._id,
            method: "PUT",
            data: JSON.stringify(brand),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.successMessage = "Başarıyla Güncellendi";
                    $scope.errorMessage = undefined;
                }
            })
    }

    $scope.remove = function (brand) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/brands/" + brand._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.brands.splice($scope.brands.indexOf(brand), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);