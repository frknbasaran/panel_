'use strict';

/* Controllers */
// signin controller
app.controller('SigninFormController', ['$rootScope', '$scope', '$http', '$state', function ($rootScope, $scope, $http, $state) {

    $scope.user = {};
    $scope.authError = null;

    localStorage.removeItem("loggedOn");
    localStorage.removeItem("user");

    $scope.login = function () {

        $scope.authError = null;

        $http.post('api/users/login', {email: $scope.user.email, password: $scope.user.password})
            .then(function (response) {
                if (response.data.error) {
                    $scope.authError = "Yönetici bilgileri hatalı";
                } else {
                    localStorage.setItem("loggedOn", true);
                    localStorage.setItem("user", JSON.stringify(response.data.data));
                    $rootScope.user = response.data.data.user;
                    $state.go('app.dashboard-v1');
                }
            }, function (x) {
                $scope.authError = 'Sunucu hatası. Sistem yöneticisyle irtibata geçmelisiniz.';
            });

    };
}])
;