app.controller('ReservationController', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {

    console.log("Reservation Controller worked.")

    if (localStorage.getItem("loggedOn") == null) {
        $location.path('/access/signin');
    } else {
        $scope.user = $rootScope.user;
    }

    $scope.newReservation = {
        "event": "",
        "user": ""
    }

    $http.get('/api/users')
        .then(function (response) {
            $scope.users = response.data;
        })

    $http.get('/api/events')
        .then(function (response) {
            $scope.events = response.data;
        })

    $http.get('/api/reservations?populate=event&populate=user')
        .then(function (response) {
            $scope.reservations = response.data;
        })

    $scope.addNewReservation = function () {

        $http({
            url: "/api/reservations",
            method: "POST",
            data: JSON.stringify($scope.newReservation),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 201) {
                    $scope.successMessage = "Başarıyla Eklendi";
                    $scope.errorMessage = undefined;
                    $scope.reservations.push(response.data);
                } else if (response.status == 200 || response.data.error == true) {
                    $scope.successMessage = undefined;
                    $scope.errorMessage = "Bir şeyler ters gitti.";
                }
            })
    }

    $scope.updateReservation = function (reservation) {

        $http({
            url: "/api/reservations/" + reservation._id,
            method: "PUT",
            data: JSON.stringify(reservation),
            headers: {"content-type": "application/json"}
        })
            .then(function (response) {
                if (response.status == 200) {
                    $scope.successMessage = "Başarıyla Güncellendi";
                    $scope.errorMessage = undefined;
                }
            })
    }

    $scope.remove = function (reservation) {
        var go = confirm("Silme işlemi geri alınamaz, emin misiniz?");
        if (go) {
            $http({
                url: "/api/reservations/" + reservation._id,
                method: "DELETE"
            })
                .then(function (response) {
                    if (response.status == 204) {
                        $scope.successMessage = "Başarıyla Silindi";
                        $scope.reservations.splice($scope.reservations.indexOf(reservation), 1);
                        $scope.errorMessage = undefined;
                    } else if (response.status == 200 || response.data.error == true) {
                        $scope.successMessage = undefined;
                        $scope.errorMessage = "Birşeyler ters gitti.";
                    }
                })
        }
    }


}]);